# QuadCirse BM Ice Nova

## Skill Tree

[WIP]https://www.pathofexile.com/passive-skill-tree/AAAABAMBAASzCPQNzQ-rEFgRDxZvFr8XLxdUGGoZiho4GmwajxzOHRQfxyKBIvQkiySqJpUnLyo4K1AreCycLYs26TpYOtg7DTwFPQ89X0SrTC1Ms05tUEJSU1M1U9RVS1WuVcZWSlgHYeJmVGpDbAttGXgvfLt85X_GghCCm4PbhMWFe4w2jYKOPI9Gj6aQVZMnl5WX9Jo7m6GbtZwynqGfAZ_foS-iAKXErJivm6-nvorAVMBmwcXDOtAf0eTVpt-w42rkIuwY7DjtPO987-vwH_ba99f60vsJ_lQ=

### Leveling

[36 Ponts](https://www.pathofexile.com/passive-skill-tree/AAAABAMBAAQHCPQQWBa_H8ci9CaVK3gsnC2LOlg9X0SrTLNQQlJTUzVUR1VLVa5VxmHiakNsC20ZghCD24w2j6aTJ5eVl_Smvq-bwFTfsA==)

#### Recommended Gear

* Quartz Scepter +1 to Level of Socketed Cold Gems
  * Ice Nova (swap in Freezing Pulse for single Target)
  * Added Cold Damage
  * Added Lightning Damage
* Quartz Scepter with good amount flat added elemental damage to spells

#### Auras

 * Clarity
 * Herald of Ice

#### Flasks

 * Life
 * Life
 * Quicksilver
 * Quicksilver
 * Mana/Hybrid

## Bandits

* Normal - Help Oak
* Cruel - Kill All (or help Alira?)
* Merciless - Kill All (or help Oak for extra Endurance Charge)

## Links

### Chest Armor - [Unique] Carcass Jack
 
 * Ice Nova
 * Cold Penetration
 * Increased Area of Effect (Concentrated Effect for higher single target DPS)
 * Blood Magic

 ???
 * Faster Casting
 * Spell Echo
 * Elemental Proliferation
 * Increased Critical Strikes
 * Controlled Destruction

### Helm - [Unique] Doedre's Scorn

 * Blasphemy
 * Temporal Chains
 * Warlord's Mark
 * Frostbite

### Boots - [Unique] Windscream

 * Cast When Damage Taken
 * Increased Duration
 * Immortal Call
 * Enfeeble

### Dagger - [Rare] Platinum Kris

 * Whirling Blades
 * Fortify
 * Faster Attacks

### Shield
 
 * Spell Totem
 * Wither
 * Increase Area of Effect

### Gloves

 * Lightning Warp
 * Faster Casting
 * Rapid Decay
 * Reduced Duration
